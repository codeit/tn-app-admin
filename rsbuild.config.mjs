import { defineConfig } from '@rsbuild/core';
import { pluginVue2 } from '@rsbuild/plugin-vue2';
import { pluginBabel } from '@rsbuild/plugin-babel';
import { pluginLess } from '@rsbuild/plugin-less';
import { pluginVue2Jsx } from '@rsbuild/plugin-vue2-jsx';
import { pluginSass } from '@rsbuild/plugin-sass';
import { pluginNodePolyfill } from '@rsbuild/plugin-node-polyfill';
export default defineConfig({
  source: {
    entry: {
      desktop: './src/desktop/index.js',
      mobile: './src/mobile/index.js',
    },
    alias: {
      '@components': './src/components',
      '@desktop': './src/desktop',
      '@mobile': './src/mobile',
    },
    define: {
      //'process.env': { ...process.env }
    },

  },
  output: {
    polyfill: 'entry',
  },
  html: {
    title({ entryName }) {
      const titles = {
        desktop: '特能集团人才测评管理系统',
        mobile: '特能集团人才测评投票系统',
      };
      return titles[entryName];
    },
  },
  dev: {
    hmr: true,
    liveReload: true,
  },
  server: {
    //open: {
    //  target: "/desktop#/",
    // },
    port: 3000,
    proxy: {
      '/api': {
        target: 'http://127.0.0.1:88',
        pathRewrite: { '^/api': '' },
      },
    },
  },
  plugins: [
    pluginBabel({
      include: /\.(?:jsx)$/,
    }),
    pluginVue2(),
    pluginVue2Jsx(),
    // pluginVueInspector(),
    pluginLess(),
    pluginSass({
      sassLoaderOptions: {
        sassOptions: {
          silenceDeprecations: ['import'],
        },
      },
    }),
    pluginNodePolyfill(),
  ],
});
