import Vue from 'vue'
import VueRouter from 'vue-router'
import { createPinia, PiniaVuePlugin } from 'pinia'
import { createPersistedState } from 'pinia-plugin-persistedstate'
import { permit } from '../directives'
Vue.use(PiniaVuePlugin)
const pinia = createPinia()
pinia.use(createPersistedState({
  storage: sessionStorage,
}))
import ElementUI from 'element-ui'
import Loading from 'vue-loading-overlay';
import { TooltipPlugin } from 'bootstrap-vue'
import 'vue-loading-overlay/dist/vue-loading.css';
import 'font-awesome/css/font-awesome.css'
import './theme/element/index.css'
import './style.scss'
import './index.css'
import locale from 'element-ui/lib/locale/lang/zh-CN'
import { router } from '../router/desktop'
import App from './App.vue'
Vue.use(VueRouter)
Vue.use(ElementUI, { size: 'small', locale });
Vue.use(Loading)
Vue.use(TooltipPlugin)
Vue.directive('permit', permit);
new Vue({
  el: '#root',
  router,
  render: (h) => h(App),
  pinia,
});


