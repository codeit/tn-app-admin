import { defineStore } from 'pinia'

export const useLoginStore = defineStore('login', {
    persist: {
        enabled: true
    },
    state: () => ({ logonId: null, user_type: -1, group_id: null, group_name: null }),
    actions: {
        setLogin(loginId) {
            this.loginId = loginId
        },
        isLogin() {
            return Boolean(this.loginId);
        }
    },
})