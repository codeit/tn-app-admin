import { useLoginStore } from "../store"

function operateNode(el, attr) {
    el.parentNode && el.parentNode.removeChild(el)
}
function hasIf(el) {
    // 子元素没有属性时直接返回
    if (!el.attributes) return
    const e = el.attributes

    for (let i = 0; i < e.length; i++) {
        // 通过正则匹配v-permit、v-else-if、v-show指令
        if (/^v\-permit$/.test(e[i].name)) {
            return e[i].name
        }
    }
}

function directHoot(el, binding) {
    const store = useLoginStore();
    const types = binding.value;
    if (types && !types.includes(store.user_type)) {
        // el.style.display = "none";
        operateNode(el, hasIf(el))
    }
}
const permit = {
    inserted: directHoot
}

export { permit }