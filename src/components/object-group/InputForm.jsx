import { FormDialog, FormLayout, FormItem, Input, Select } from '@formily/element'
import { createSchemaField } from '@formily/vue'
const { SchemaField } = createSchemaField({
    components: {
        FormItem,
        Input,
        Select,
    },
})

// 弹框表单组件
export const InputForm = {
    data() {
        const schema = {
            type: 'object',
            properties: {
                name: {
                    type: 'string',
                    title: '名称',
                    required: true,
                    'x-decorator': 'FormItem',
                    'x-component': 'Input',
                    'x-component-props': {
                        size: 'small'
                    }
                },
                short_name: {
                    type: 'string',
                    title: '简称',
                    required: true,
                    'x-decorator': 'FormItem',
                    'x-component': 'Input',
                    'x-component-props': {
                        size: 'small'
                    }
                },
                type: {
                    type: 'string',
                    title: '单位类型',
                    required: true,
                    'x-decorator': 'FormItem',
                    'x-component': 'Select',
                    'x-component-props': {
                        size: 'small'
                    },
                    enum: [
                        { label: "请选择.", value: "" },
                        { label: "子集团", value: "子集团" },
                        { label: "提级管理", value: "提级管理" },
                    ]
                },
                remark: {
                    type: 'string',
                    title: '备注',
                    'x-decorator': 'FormItem',
                    'x-component': 'Input.TextArea',
                    'x-component-props': {
                        size: 'small'
                    }
                },
            },
        }
        return {
            schema,
        }
    },
    render(h) {
        return (
            <FormLayout labelCol={6} wrapperCol={10}>
                <SchemaField schema={this.schema} />
                <FormDialog.Footer>
                </FormDialog.Footer>
            </FormLayout>
        )
    },
}