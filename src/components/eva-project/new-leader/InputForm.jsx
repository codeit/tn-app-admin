import { FormDialog, FormLayout, FormItem, Input, Radio, DatePicker } from '@formily/element'
import { createSchemaField } from '@formily/vue'

const { SchemaField } = createSchemaField({
    components: {
        FormItem,
        Input,
        Radio,
        DatePicker,
    },
})

// 弹框表单组件
export const InputForm = {
    data() {
        const schema = {
            type: 'object',
            properties: {
                name: {
                    type: 'string',
                    title: '姓名',
                    required: true,
                    'x-decorator': 'FormItem',
                    'x-component': 'Input',
                    'x-component-props': {
                        size: 'small'
                    }
                },
                unique_id: {
                    type: 'string',
                    title: '唯一标识',
                    'x-decorator': 'FormItem',
                    'x-component': 'Input',
                    'x-component-props': {
                        size: 'small'
                    }
                },
                gender: {
                    type: 'string',
                    title: '性别',
                    required: true,
                    'x-decorator': 'FormItem',
                    'x-component': 'Radio.Group',
                    'x-component-props': {
                        size: 'small'
                    },
                    enum: [
                        { label: "男", value: "男" },
                        { label: "女", value: "女" },
                    ]
                },
                bod: {
                    type: 'string',
                    title: '出生年月',
                    'x-decorator': 'FormItem',
                    'x-component': 'DatePicker',
                    'x-component-props': {
                        type: 'month',
                    },
                },
                duty_prev: {
                    type: 'string',
                    title: '原任职务',
                    required: true,
                    'x-decorator': 'FormItem',
                    'x-component': 'Input.TextArea',
                    'x-component-props': {
                        size: 'small'
                    },
                },
                duty_current: {
                    type: 'string',
                    title: '现任职务',
                    required: true,
                    'x-decorator': 'FormItem',
                    'x-component': 'Input.TextArea',
                    'x-component-props': {
                        size: 'small'
                    },
                },
                duty_date: {
                    type: 'string',
                    title: '任职日期',
                    'x-decorator': 'FormItem',
                    'x-component': 'DatePicker',
                    'x-component-props': {
                        type: 'month',
                    },
                },
                remark: {
                    type: 'string',
                    title: '备注',
                    'x-decorator': 'FormItem',
                    'x-component': 'Input.TextArea',
                    'x-component-props': {
                        size: 'small'
                    }
                },
            },
        }
        return {
            schema,
        }
    },
    render(h) {
        return (
            <FormLayout labelCol={6} wrapperCol={10}>
                <SchemaField schema={this.schema} />
                <FormDialog.Footer>
                </FormDialog.Footer>
            </FormLayout>
        )
    },
}