import { FormDialog, FormLayout, FormItem, Input, Radio, DatePicker } from '@formily/element'
import { createSchemaField } from '@formily/vue'

const { SchemaField } = createSchemaField({
    components: {
        FormItem,
        Input,
        Radio,
        DatePicker,
    },
})

// 弹框表单组件
export const InputForm = {
    data() {
        const schema = {
            type: 'object',
            properties: {
                name: {
                    type: 'string',
                    title: '姓名',
                    required: true,
                    'x-decorator': 'FormItem',
                    'x-component': 'Input',
                    'x-component-props': {
                        size: 'small'
                    }
                },
                unique_id: {
                    type: 'string',
                    title: '唯一标识',
                    'x-decorator': 'FormItem',
                    'x-component': 'Input',
                    'x-component-props': {
                        size: 'small'
                    }
                },
                bod: {
                    type: 'string',
                    title: '出生年月',
                    'x-decorator': 'FormItem',
                    'x-component': 'DatePicker',
                    'x-component-props': {
                        type: 'month',
                    },
                },
                role_type: {
                    type: 'string',
                    title: '职务类别',
                    required: true,
                    'x-decorator': 'FormItem',
                    'x-component': 'Radio.Group',
                    'x-component-props': {
                        size: 'small',
                        optionType: 'button',
                    },
                    enum: [
                        { label: "专职外部董事", value: "专职外部董事" },
                        { label: "兼职外部董事", value: "兼职外部董事" },
                    ]
                },
                remark: {
                    type: 'string',
                    title: '备注',
                    'x-decorator': 'FormItem',
                    'x-component': 'Input.TextArea',
                    'x-component-props': {
                        size: 'small'
                    }
                },
                project_id: {
                    type: 'string',
                    hidden: true,
                },
            },
        }
        return {
            schema,
        }
    },
    render(h) {
        return (
            <FormLayout labelCol={6} wrapperCol={10}>
                <SchemaField schema={this.schema} />
                <FormDialog.Footer>
                </FormDialog.Footer>
            </FormLayout>
        )
    },
}