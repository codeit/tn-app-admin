import { FormDialog, FormLayout, FormItem, Select } from '@formily/element'
import { createSchemaField } from '@formily/vue'
import { action } from '@formily/reactive'
import { BaseApi } from '../../../api/common/base-api'
import { buildVirtualValue } from '../../../api/common/util'

const useRuFormDataSource = (service) => (field) => {
    field.loading = true
    service(field).then(
        action.bound((data) => {
            field.dataSource = data
            field.loading = false
        })
    )
}

const loadRuForm = async (field) => {
    const api = new BaseApi({ prefix: "/ru-form" })
    const data = await api.findList({ order: [["sort"]] });
    return new Promise((resolve) => {
        resolve(data.data.map(({ id, name, user_type }) => ({ label: name, value: buildVirtualValue([id, name, user_type]) })))
    });
}


const { SchemaField } = createSchemaField({
    components: {
        FormItem,
        Select,
    },
    scope: {
        useRuFormDataSource,
        loadRuForm
    },
})

// 弹框表单组件
export const InputForm = {
    data() {
        const schema = {
            type: 'object',
            properties: {
                ru_form_info: {
                    type: 'string',
                    title: '可选评测表',
                    required: true,
                    'x-decorator': 'FormItem',
                    'x-component': 'Select',
                    'x-component-props': {
                        size: 'small'
                    },
                    'x-reactions': ['{{useRuFormDataSource(loadRuForm)}}'],
                },
                project_id: {
                    type: 'string',
                    hidden: true,
                },
                ru_form_id: {
                    type: 'string',
                    visible: false,
                },
                ru_form_name: {
                    type: 'string',
                    visible: false,
                },
                user_type: {
                    type: 'string',
                    visible: false,
                }
            },
        }
        return {
            schema,
        }
    },
    render(h) {
        return (
            <FormLayout labelCol={6} wrapperCol={10}>
                <SchemaField schema={this.schema} />
                <FormDialog.Footer>
                </FormDialog.Footer>
            </FormLayout>
        )
    },
}