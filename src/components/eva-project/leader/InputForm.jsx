import { FormDialog, FormLayout, FormItem, Input, Select, DatePicker, InputNumber } from '@formily/element'
import { createSchemaField } from '@formily/vue'
import { action } from '@formily/reactive'
import { BaseApi } from '../../../api/common/base-api'

const useOwnerTypeDataSource = (service) => (field) => {
    field.loading = true
    service(field).then(
        action.bound((data) => {
            field.dataSource = data
            field.loading = false
        })
    )
}

const loadOwnerType = async (field) => {
    const api = new BaseApi({ prefix: "/owner-type" })
    const data = await api.findList({ order: [['sort']] });
    return new Promise((resolve) => {
        resolve(data.data.map(({ id, name, remark }) => ({ label: `${name}-${remark}`, value: `${id}^${name}` })))
    });
}


const { SchemaField } = createSchemaField({
    components: {
        FormItem,
        Input,
        Select,
        DatePicker,
        InputNumber,
    },
    scope: {
        useOwnerTypeDataSource,
        loadOwnerType
    },
})

// 弹框表单组件
export const InputForm = {
    data() {
        const schema = {
            type: 'object',
            properties: {
                name: {
                    type: 'string',
                    title: '姓名',
                    required: true,
                    'x-decorator': 'FormItem',
                    'x-component': 'Input',
                    'x-component-props': {
                        size: 'small'
                    }
                },
                unique_id: {
                    type: 'string',
                    title: '唯一标识',
                    'x-decorator': 'FormItem',
                    'x-component': 'Input',
                    'x-component-props': {
                        size: 'small'
                    }
                },
                category: {
                    type: 'string',
                    title: '类别',
                    required: true,
                    'x-decorator': 'FormItem',
                    'x-component': 'Select',
                    'x-component-props': {
                        size: 'small'
                    },
                    enum: [
                        { label: "正职领导", value: "正职领导" },
                        { label: "其他领导", value: "其他领导" },
                    ]
                },
                type_info: {
                    type: 'string',
                    title: '人员类型',
                    required: true,
                    'x-decorator': 'FormItem',
                    'x-component': 'Select',
                    'x-component-props': {
                        size: 'small'
                    },
                    'x-reactions': ['{{useOwnerTypeDataSource(loadOwnerType)}}'],
                },
                role_name: {
                    type: 'number',
                    title: '职务',
                    required: true,
                    'x-decorator': 'FormItem',
                    'x-component': 'Input',
                    'x-component-props': {
                        size: 'small'
                    },
                },
                bod: {
                    type: 'string',
                    title: '出生年月',
                    'x-decorator': 'FormItem',
                    'x-component': 'DatePicker',
                    'x-component-props': {
                        type: 'month',
                    },
                },
                remark: {
                    type: 'string',
                    title: '备注',
                    'x-decorator': 'FormItem',
                    'x-component': 'Input.TextArea',
                    'x-component-props': {
                        size: 'small'
                    }
                },
                project_id: {
                    type: 'string',
                    hidden: true,
                },
                type_id: {
                    type: 'string',
                    hidden: true,
                },
                type_name: {
                    type: 'string',
                    hidden: true,
                }
            },
        }
        return {
            schema,
        }
    },
    render(h) {
        return (
            <FormLayout labelCol={6} wrapperCol={10}>
                <SchemaField schema={this.schema} />
                <FormDialog.Footer>
                </FormDialog.Footer>
            </FormLayout>
        )
    },
}