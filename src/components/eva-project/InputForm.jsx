import { FormDialog, FormLayout, FormItem, Input, DatePicker, Select, Radio } from '@formily/element'
import { createSchemaField } from '@formily/vue'
import { action } from '@formily/reactive'
import { BaseApi } from '../../api/common/base-api'

const useObjectGroupDataSource = (service) => (field) => {
    field.loading = true
    service(field).then(
        action.bound((data) => {
            field.dataSource = data
            field.loading = false
        })
    )
}

const loadObjectGroup = async (field) => {
    const api = new BaseApi({ prefix: "/object-group" })
    const data = await api.findList({ order: [['sort']] });
    return new Promise((resolve) => {
        resolve(data.data.map(({ id, name }) => ({ label: name, value: `${id}^${name}` })))
    });
}


const { SchemaField } = createSchemaField({
    components: {
        FormItem,
        Input,
        DatePicker,
        Select,
        Radio,
    },

    scope: {
        useObjectGroupDataSource,
        loadObjectGroup,
    }
})

// 弹框表单组件
export const InputForm = {
    data() {
        const schema = {
            type: 'object',
            properties: {
                object_group_info: {
                    type: 'string',
                    title: '测评单位',
                    required: true,
                    'x-decorator': 'FormItem',
                    'x-component': 'Select',
                    'x-component-props': {
                        size: 'small'
                    },
                    'x-reactions': ['{{useObjectGroupDataSource(loadObjectGroup)}}'],
                },
                name: {
                    type: 'string',
                    title: '名称',
                    required: true,
                    'x-decorator': 'FormItem',
                    'x-component': 'Input',
                    'x-component-props': {
                        size: 'small'
                    }
                },
                group_type: {
                    type: 'string',
                    title: '企业类型',
                    required: true,
                    'x-decorator': 'FormItem',
                    'x-component': 'Radio.Group',
                    'x-component-props': {
                        size: 'small',
                        border: true,
                        optionType: 'button',
                    },
                    enum: [
                        { label: "有董事会", value: 1 },
                        { label: "无董事会", value: 0 },
                    ]
                },
                start_time: {
                    type: 'string',
                    title: '开始时间',
                    required: true,
                    'x-decorator': 'FormItem',
                    'x-component': 'DatePicker',
                },
                end_time: {
                    type: 'string',
                    title: '结束时间',
                    required: true,
                    'x-decorator': 'FormItem',
                    'x-component': 'DatePicker',
                },
                ef_year: {
                    type: 'string',
                    title: '年份',
                    required: true,
                    'x-decorator': 'FormItem',
                    'x-component': 'DatePicker',
                    'x-component-props': {
                        type: 'year',
                    },
                },
                remark: {
                    type: 'string',
                    title: '备注',
                    'x-decorator': 'FormItem',
                    'x-component': 'Input.TextArea',
                },
                object_group_id: {
                    type: 'string',
                    hidden: true,
                },
                object_group_name: {
                    type: 'string',
                    hidden: true,
                },
            },
        }
        return {
            schema,
        }
    },
    render(h) {
        return (
            <FormLayout labelCol={6} wrapperCol={10}>
                <SchemaField schema={this.schema} />
                <FormDialog.Footer>
                </FormDialog.Footer>
            </FormLayout>
        )
    },
}