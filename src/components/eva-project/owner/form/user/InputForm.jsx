
import { FormDialog, FormLayout, FormItem, Checkbox } from '@formily/element'
import { createSchemaField } from '@formily/vue'
import { action } from '@formily/reactive'
import { BaseApi } from "../../../../../api/common/base-api"
const userPrefixs = [
    "/eva-project-leader",
    "/eva-project-new-leader",
    "/eva-project-ext-director"];
const useEvaProjectUserDataSource = (service) => (field) => {
    field.loading = true
    service(field).then(
        action.bound((data) => {
            field.dataSource = data
            field.loading = false
        })
    )
}

const loadEvaProjectUser = async (field) => {
    const project_id = field.form.query("project_id").take();
    const user_type = field.form.query("user_type").take();
    if (project_id && user_type) {
        const prefix = userPrefixs[parseInt(user_type.value)];
        const api = new BaseApi({ prefix })
        const data = await api.findList({ where: { project_id: project_id.value } });
        return new Promise((resolve) => {
            resolve(data.data.map(({ id, name }) => ({ label: name, value: `${id}^${name}` })))
        });
    }
    return new Promise((resolve) => resolve([]));

}


const { SchemaField } = createSchemaField({
    components: {
        FormItem,
        Checkbox,
    },
    scope: {
        useEvaProjectUserDataSource,
        loadEvaProjectUser,
    }
})

// 弹框表单组件
export const InputForm = {
    data() {
        const schema = {
            type: 'object',
            properties: {
                user_info: {
                    type: 'array',
                    title: '测评对象人员',
                    required: true,
                    'x-decorator': 'FormItem',
                    'x-component': 'Checkbox.Group',
                    'x-component-props': {
                        size: 'small'
                    },
                    'x-reactions': ['{{useEvaProjectUserDataSource(loadEvaProjectUser)}}'],
                },
                project_id: {
                    type: "string",
                    visible: false,
                },
                project_owner_form_id: {
                    type: "string",
                    visible: false,
                },
                user_type: {
                    type: "number",
                    visible: false,
                }

            },
        }
        return {
            schema,
        }
    },
    render(h) {
        return (
            <FormLayout labelCol={6} wrapperCol={18}>
                <SchemaField schema={this.schema} />
                <FormDialog.Footer>
                </FormDialog.Footer>
            </FormLayout>
        )
    },
}