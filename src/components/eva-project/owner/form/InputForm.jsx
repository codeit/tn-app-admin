
import { FormDialog, FormLayout, FormItem, InputNumber, Select, Input } from '@formily/element'
import { createSchemaField } from '@formily/vue'
import { action } from '@formily/reactive'
import { BaseApi } from "../../../../api/common/base-api"

const useRuFormDataSource = (service) => (field) => {
    field.loading = true
    service(field).then(
        action.bound((data) => {
            field.dataSource = data
            field.loading = false
        })
    )
}

const loadRuForm = async (field) => {
    const api = new BaseApi({ prefix: "/ru-form" })
    const data = await api.findList({ order: "['sort']" });
    return new Promise((resolve) => {
        resolve(data.data.map(({ id, name }) => ({ label: name, value: `${id}^${name}` })))
    });
}


const { SchemaField } = createSchemaField({
    components: {
        FormItem,
        Select,
        Input,
        InputNumber,
    },
    scope: {
        useRuFormDataSource,
        loadRuForm,
    }
})

// 弹框表单组件
export const InputForm = {
    data() {
        const schema = {
            type: 'object',
            properties: {
                ru_form_info: {
                    type: 'string',
                    title: '测评表名称',
                    required: true,
                    'x-decorator': 'FormItem',
                    'x-component': 'Select',
                    'x-component-props': {
                        size: 'small'
                    },
                    'x-reactions': ['{{useRuFormDataSource(loadRuForm)}}'],
                },
                remark: {
                    type: 'string',
                    title: '备注',
                    'x-decorator': 'FormItem',
                    'x-component': 'Input.TextArea',
                    'x-component-props': {
                        size: 'small'
                    }
                },
                ru_form_id: {
                    type: "string",
                    hidden: true,
                },
                ru_form_name: {
                    type: "string",
                    hidden: true
                }
            },
        }
        return {
            schema,
        }
    },
    render(h) {
        return (
            <FormLayout labelCol={6} wrapperCol={10}>
                <SchemaField schema={this.schema} />
                <FormDialog.Footer>
                </FormDialog.Footer>
            </FormLayout>
        )
    },
}