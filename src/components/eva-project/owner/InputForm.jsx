
import { FormDialog, FormLayout, FormItem, InputNumber, Select, Input } from '@formily/element'
import { createSchemaField } from '@formily/vue'
import { action } from '@formily/reactive'
import { OwnerNameApi } from '../../../api/owner-name'

const api = new OwnerNameApi
const useOwnerNameDataSource = (service) => (field) => {
    field.loading = true
    service(field).then(
        action.bound((data) => {
            field.dataSource = data
            field.loading = false
        })
    )
}

const loadOwnerName = async (field) => {
    const project_field = field.form.query("project_id").take();
    const where = {};
    if (project_field) {
        where.project_id = project_field.value;
        const data = await api.findOwnersByProject({ project_id: project_field.value });
        return new Promise((resolve) => {
            resolve(data.data.map(({ id, name, remark }) => ({ label: `${name}-${remark}`, value: `${id}^${name}` })))
        })
    }
    return Promise.resolve([]);
}


const { SchemaField } = createSchemaField({
    components: {
        FormItem,
        Select,
        Input,
        InputNumber,
    },
    scope: {
        useOwnerNameDataSource,
        loadOwnerName,
    }
})

// 弹框表单组件
export const InputForm = {
    data() {
        const schema = {
            type: 'object',
            properties: {
                owner_name_info: {
                    type: 'string',
                    title: '主体名称',
                    required: true,
                    'x-decorator': 'FormItem',
                    'x-component': 'Select',
                    'x-component-props': {
                        size: 'small'
                    },
                    'x-reactions': ['{{useOwnerNameDataSource(loadOwnerName)}}'],
                },
                plan_quantity: {
                    type: 'number',
                    title: '计划数量',
                    required: true,
                    'x-decorator': 'FormItem',
                    'x-component': 'InputNumber',
                    'x-component-props': {
                        size: 'small',
                        placeholder: '计划数量'
                    }
                },
                remark: {
                    type: 'string',
                    title: '备注',
                    'x-decorator': 'FormItem',
                    'x-component': 'Input.TextArea',
                    'x-component-props': {
                        size: 'small'
                    }
                },
                project_id: {
                    type: 'string',
                    hidden: true,
                },
                group_type: {
                    type: 'number',
                    hidden: true,
                },
                owner_name_id: {
                    type: "string",
                    hidden: true,
                },
                owner_name_name: {
                    type: "string",
                    hidden: true
                }
            },
        }
        return {
            schema,
        }
    },
    render(h) {
        return (
            <FormLayout labelCol={6} wrapperCol={10}>
                <SchemaField schema={this.schema} />
                <FormDialog.Footer>
                </FormDialog.Footer>
            </FormLayout>
        )
    },
}