import { FormDialog, FormLayout, FormItem, Input, InputNumber, Select } from '@formily/element'
import { createSchemaField } from '@formily/vue'
import { action } from '@formily/reactive'
import { BaseApi } from '../../api/common/base-api'

const useOwnerTypeDataSource = (service) => (field) => {
    field.loading = true
    service(field).then(
        action.bound((data) => {
            field.dataSource = data
            field.loading = false
        })
    )
}

const loadOwnerType = async (field) => {
    const api = new BaseApi({ prefix: "/owner-type" })
    const data = await api.findList({ order: [['name'], ["sort"]] });
    return new Promise((resolve) => {
        resolve(data.data.map(({ id, name, remark }) => ({ label: `${name}-${remark}`, value: `${id}^${name}` })))
    });
}


const { SchemaField } = createSchemaField({
    components: {
        FormItem,
        Input,
        Select,
        InputNumber,
    },
    scope: {
        useOwnerTypeDataSource,
        loadOwnerType
    },
})

// 弹框表单组件
export const InputForm = {
    data() {
        const schema = {
            type: 'object',
            properties: {
                name: {
                    type: 'string',
                    title: '名称',
                    required: true,
                    'x-decorator': 'FormItem',
                    'x-component': 'Input',
                    'x-component-props': {
                        size: 'small'
                    }
                },
                type_info: {
                    type: 'string',
                    title: '所属票类',
                    required: true,
                    'x-decorator': 'FormItem',
                    'x-component': 'Select',
                    'x-component-props': {
                        size: 'small'
                    },
                    'x-reactions': ['{{useOwnerTypeDataSource(loadOwnerType)}}'],
                },
                add_ticks: {
                    type: 'number',
                    title: '追加票数',
                    'x-decorator': 'FormItem',
                    'x-component': 'InputNumber',
                    'x-component-props': {
                        size: 'small'
                    },
                },
                remark: {
                    type: 'string',
                    title: '备注',
                    'x-decorator': 'FormItem',
                    'x-component': 'Input.TextArea',
                    'x-component-props': {
                        size: 'small'
                    }
                },
                type_id: {
                    type: 'string',
                    hidden: true,
                },
                type_name: {
                    type: 'string',
                    hidden: true,
                }
            },
        }
        return {
            schema,
        }
    },
    render(h) {
        return (
            <FormLayout labelCol={6} wrapperCol={10}>
                <SchemaField schema={this.schema} />
                <FormDialog.Footer>
                </FormDialog.Footer>
            </FormLayout>
        )
    },
}