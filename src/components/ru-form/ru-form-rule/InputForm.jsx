
import { FormDialog, FormLayout, FormItem, Input, Radio, InputNumber, Select, } from '@formily/element'
import { createSchemaField } from '@formily/vue'
import { action } from '@formily/reactive'
import { RuSecondRuleApi } from '../../../api/ru-second-rule'

const useSecondRuleDataSource = (service) => (field) => {
    field.loading = true
    service(field).then(
        action.bound((data) => {
            field.dataSource = data
            field.loading = false
        })
    )
}

const loadSecondRule = async (field) => {
    const api = new RuSecondRuleApi();
    const data = await api.findList({ order: [['top_id'], ['sort']] });
    return new Promise((resolve) => {
        resolve(data.data.map(({ id, name, top_id, top_name }) => ({ label: `${name}-${top_name}`, value: `${id}^${name}^${top_id}^${top_name}` })))
    });
}


const { SchemaField } = createSchemaField({
    components: {
        FormItem,
        Input,
        Radio,
        InputNumber,
        Select,
    },
    scope: {
        useSecondRuleDataSource,
        loadSecondRule,
    }
})

// 弹框表单组件
export const InputForm = {
    data() {
        const schema = {
            type: 'object',
            properties: {
                second_info: {
                    type: 'string',
                    title: '二级指标',
                    required: true,
                    'x-decorator': 'FormItem',
                    'x-component': 'Select',
                    'x-component-props': {
                        size: 'small'
                    },
                    'x-reactions': ['{{useSecondRuleDataSource(loadSecondRule)}}'],
                },
                top_name: {
                    type: 'string',
                    title: '一级指标',
                    readOnly: true,
                    'x-decorator': 'FormItem',
                    'x-component': 'Input',
                    'x-component-props': {
                        size: 'small',
                        placeholder: '请选择二级指标'
                    }
                },
                use_weight: {
                    type: 'boolean',
                    title: '是否权重',
                    'x-decorator': 'FormItem',
                    'x-component': 'Radio.Group',
                    'x-component-props': {
                        size: 'small',
                        optionType: 'button',
                    },
                    enum: [
                        { label: "是", value: "是" },
                        { label: "否", value: "否" },
                    ]
                },
                weight_value: {
                    type: "number",
                    title: '权重值',
                    'x-decorator': 'FormItem',
                    'x-component': 'InputNumber',
                    'x-component-props': {
                        precision: '1',
                        step: '0.1',
                        size: 'small'
                    },
                    'x-reactions': {
                        dependencies: ['.use_weight'],
                        when: '{{$deps[0]==="是"}}',
                        fulfill: {
                            schema: {
                                'x-visible': true,
                                required: true,
                            },
                        },
                        otherwise: {
                            schema: {
                                'x-visible': false,
                                required: false,
                            },
                        }
                    }
                },
                remark: {
                    type: 'string',
                    title: '备注',
                    'x-decorator': 'FormItem',
                    'x-component': 'Input.TextArea',
                    'x-component-props': {
                        size: 'small'
                    }
                },
                top_id: {
                    type: "string",
                    hidden: true
                },
                second_id: {
                    type: 'string',
                    hidden: true,
                },
                second_name: {
                    type: 'string',
                    hidden: true,
                }
            },
        }
        return {
            schema,
        }
    },
    render(h) {
        return (
            <FormLayout labelCol={6} wrapperCol={10}>
                <SchemaField schema={this.schema} />
                <FormDialog.Footer>
                </FormDialog.Footer>
            </FormLayout>
        )
    },
}