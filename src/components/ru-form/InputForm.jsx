import { FormDialog, FormLayout, FormItem, Input, Radio } from '@formily/element'
import { createSchemaField } from '@formily/vue'

const { SchemaField } = createSchemaField({
    components: {
        FormItem,
        Input,
        Radio,
    },
})

// 弹框表单组件
export const InputForm = {
    data() {
        const schema = {
            type: 'object',
            properties: {
                name: {
                    type: 'string',
                    title: '名称',
                    required: true,
                    'x-decorator': 'FormItem',
                    'x-component': 'Input',
                    'x-component-props': {
                        size: 'small'
                    }
                },
                user_type: {
                    type: 'string',
                    title: '测评表类型',
                    required: true,
                    'x-decorator': 'FormItem',
                    'x-component': 'Radio.Group',
                    'x-component-props': {
                        optionType: 'button',
                    },
                    enum: [
                        { label: "领导人员", value: "0" },
                        { label: "新提拔干部", value: "1" },
                        { label: "外部董事", value: "2" },
                        { label: "领导班子", value: "3" },
                        { label: "选人用人", value: "4" },
                        { label: "政治素质", value: "5" },
                    ]
                },
                use_entire: {
                    type: 'string',
                    title: '整体评价',
                    required: true,
                    'x-decorator': 'FormItem',
                    'x-component': 'Radio.Group',
                    'x-component-props': {
                        optionType: 'button',
                    },
                    enum: [
                        { label: "是", value: "1" },
                        { label: "否", value: "0" },
                    ]
                },
                entire_title: {
                    type: 'string',
                    title: '评价内容',
                    visible: false,
                    'x-decorator': 'FormItem',
                    'x-component': 'Input',
                    'x-component-props': {
                        size: 'small'
                    },
                    'x-reactions': {
                        dependencies: ['.use_entire'],
                        when: '{{$deps[0]==1}}',
                        fulfill: {
                            schema: {
                                'x-visible': true,
                                required: true
                            },
                        },
                        otherwise: {
                            schema: {
                                'x-visible': false,
                                required: false
                            },
                        }
                    }
                },
                remark: {
                    type: 'string',
                    title: '备注',
                    'x-decorator': 'FormItem',
                    'x-component': 'Input.TextArea',
                    'x-component-props': {
                        size: 'small'
                    }
                },
            },
        }
        return {
            schema,
        }
    },
    render(h) {
        return (
            <FormLayout labelCol={4} wrapperCol={18}>
                <SchemaField schema={this.schema} />
                <FormDialog.Footer>
                </FormDialog.Footer>
            </FormLayout>
        )
    },
}