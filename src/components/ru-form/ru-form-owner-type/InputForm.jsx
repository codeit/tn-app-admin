
import { FormDialog, FormLayout, FormItem, InputNumber, Select, Input } from '@formily/element'
import { createSchemaField } from '@formily/vue'
import { action } from '@formily/reactive'
import { BaseApi } from "../../../api/common/base-api"
const useOwnerTypeDataSource = (service) => (field) => {
    field.loading = true
    service(field).then(
        action.bound((data) => {
            field.dataSource = data
            field.loading = false
        })
    )
}

const loadOwnerType = async (field) => {
    const api = new BaseApi({ prefix: "/owner-type" })
    const data = await api.findList({ order: [['name']] });
    return new Promise((resolve) => {
        resolve(data.data.map(({ id, name, remark }) => ({ label: `${name}-${remark}`, value: `${id}^${name}` })))
    });
}


const { SchemaField } = createSchemaField({
    components: {
        FormItem,
        Select,
        Input,
        InputNumber,
    },
    scope: {
        useOwnerTypeDataSource,
        loadOwnerType,
    }
})

// 弹框表单组件
export const InputForm = {
    data() {
        const schema = {
            type: 'object',
            properties: {
                type_info: {
                    type: 'string',
                    title: '主体类别',
                    required: true,
                    'x-decorator': 'FormItem',
                    'x-component': 'Select',
                    'x-component-props': {
                        size: 'small'
                    },
                    'x-reactions': ['{{useOwnerTypeDataSource(loadOwnerType)}}'],
                },
                weight_value: {
                    type: 'number',
                    title: '权重',
                    required: true,
                    'x-decorator': 'FormItem',
                    'x-component': 'InputNumber',
                    'x-component-props': {
                        size: 'small',
                        placeholder: '请输入权重'
                    }
                },
                remark: {
                    type: 'string',
                    title: '备注',
                    'x-decorator': 'FormItem',
                    'x-component': 'Input.TextArea',
                    'x-component-props': {
                        size: 'small'
                    }
                },
                group_type: {
                    type: 'string',
                    hidden: true,
                },
                type_id: {
                    type: "string",
                    hidden: true,
                },
                type_name: {
                    type: "string",
                    hidden: true
                }
            },
        }
        return {
            schema,
        }
    },
    render(h) {
        return (
            <FormLayout labelCol={6} wrapperCol={10}>
                <SchemaField schema={this.schema} />
                <FormDialog.Footer>
                </FormDialog.Footer>
            </FormLayout>
        )
    },
}