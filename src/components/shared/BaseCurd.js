import { useLoginStore } from "../../store";
const sharedConfig = {
    methods: {
        async _saveBatch({ api, records }) {
            const result = await api.saveBatch({ records });
            return result;
        },
        async _save({ api, values }) {
            const result = await api.saveOrUpdate({ values });
            return result;
        },
        async _remove({ api, where }) {
            const result = await api.remove({ where });
            return result;
        },
        async handPage(page) {
            await this.loadPager({ page });
        },
        allowed(vals) {
            const store = useLoginStore();
            return vals && vals.includes(store.user_type);
        },
        async sort(api, id, sort, delta) {
            const vsort = (sort + delta) <= 0 ? 0 : (sort + delta);
            const where = {};
            if (typeof (id) === 'string') {
                where = { id };
            } else if (typeof (id) === 'object') {
                where = { ...id };
            }
            const result = await api.update({ values: { sort: vsort }, where })
            this.$message({
                message: result.message,
                type: 'success',
                center: true
            });
            this.loadPager({ page: 1 });
        },

        gotoRoute(path) {
            this.$router.push({ path });
        }

    }
}
export { sharedConfig }