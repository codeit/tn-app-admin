import { FormDialog, FormLayout, FormItem, Input, Select, Radio, InputNumber, } from '@formily/element'
import { action } from '@formily/reactive'
import { createSchemaField } from '@formily/vue'
import { BaseApi } from '../../api/common/base-api'

const useTopRuleDataSource = (service) => (field) => {
    field.loading = true
    service(field).then(
        action.bound((data) => {
            field.dataSource = data
            field.loading = false
        })
    )
}

const loadTopRule = async (field) => {
    const api = new BaseApi({ prefix: "/ru-top-rule" })
    const data = await api.findList({ order: [['sort']] });
    return new Promise((resolve) => {
        resolve(data.data.map(({ id, name }) => ({ label: name, value: `${id}^${name}` })))
    });
}


const { SchemaField } = createSchemaField({
    components: {
        FormItem,
        Input,
        Select,
        Radio,
        InputNumber,
    },
    scope: {
        useTopRuleDataSource,
        loadTopRule,
    }
})




// 弹框表单组件
export const InputForm = {
    data() {
        const schema = {
            type: 'object',
            properties: {
                name: {
                    type: 'string',
                    title: '名称',
                    required: true,
                    'x-decorator': 'FormItem',
                    'x-component': 'Input',
                    'x-component-props': {
                        size: 'small'
                    }
                },
                top_info: {
                    type: 'string',
                    title: '一级指标',
                    required: true,
                    'x-decorator': 'FormItem',
                    'x-component': 'Select',
                    'x-component-props': {
                        size: 'small'
                    },
                    'x-reactions': ['{{useTopRuleDataSource(loadTopRule)}}'],
                },
                eva_target: {
                    type: 'string',
                    title: '评价对象',
                    required: true,
                    'x-decorator': 'FormItem',
                    'x-component': 'Radio.Group',
                    'x-component-props': {
                        size: 'small',
                        optionType: 'button',
                    },
                    enum: [
                        { label: "单位", value: "单位" },
                        { label: "人员", value: "人员" },
                    ]
                },
                rule_type: {
                    type: 'string',
                    title: '指标类型',
                    required: true,
                    'x-decorator': 'FormItem',
                    'x-component': 'Radio.Group',
                    'x-component-props': {
                        size: 'small',
                        optionType: 'button',
                    },
                    enum: [
                        { label: "打分", value: "打分" },
                        { label: "单选", value: "单选" },
                        { label: "多选", value: "多选" },
                        { label: "问答", value: "问答" },
                    ]
                },
                is_required: {
                    type: 'boolean',
                    title: '是否必填',
                    required: true,
                    enum: [
                        {
                            label: '是',
                            value: true,
                        },
                        {
                            label: '否',
                            value: false,
                        },
                    ],
                    'x-decorator': 'FormItem',
                    'x-component': 'Radio.Group',
                    'x-component-props': {
                        optionType: 'button',
                    },
                },
                options: {
                    type: 'string',
                    title: '选项',
                    required: true,
                    display: 'none',
                    visible: false,
                    'x-decorator': 'FormItem',
                    'x-component': 'Input.TextArea',
                    'x-component-props': {
                        rows: 5,
                        size: 'small',
                        placeholder: "每行一个"
                    },
                    'x-reactions': {
                        dependencies: ['.rule_type'],
                        when: '{{["打分","单选","多选"].find(d=>d===$deps[0])}}',
                        fulfill: {
                            schema: {
                                'x-visible': true,
                                'default': '10\n9\n8\n7\n6\n5\n4\n3\n2\n1'
                            },
                        },
                        otherwise: {
                            schema: {
                                'default': '',
                            },
                        }
                    }

                },
                values: {
                    type: 'string',
                    title: '选项值',
                    display: 'none',
                    visible: false,
                    'x-decorator': 'FormItem',
                    'x-component': 'Input.TextArea',
                    'x-component-props': {
                        rows: 5,
                        size: 'small',
                        placeholder: "每行一个"
                    },
                    'x-reactions': {
                        dependencies: ['.rule_type'],
                        when: '{{["打分","单选"].find(d=>d===$deps[0])}}',
                        fulfill: {
                            schema: {
                                'x-visible': false,
                            },
                        },
                        otherwise: {
                            schema: {
                                'x-visible': false,
                            },
                        }
                    }
                },
                min_select: {
                    type: 'string',
                    title: '最少选择',
                    display: 'none',
                    visible: false,
                    required: true,
                    'x-decorator': 'FormItem',
                    'x-component': 'InputNumber',
                    'x-component-props': {
                        size: 'small'
                    },
                    'x-reactions': {
                        dependencies: ['.rule_type'],
                        when: '{{$deps[0]==="多选"}}',
                        fulfill: {
                            schema: {
                                'x-visible': true,
                            },
                        },
                        otherwise: {
                            schema: {
                                'x-visible': false,
                            },
                        }
                    }
                },
                max_select: {
                    type: 'string',
                    title: '最多选择',
                    display: 'none',
                    visible: false,
                    required: true,
                    'x-decorator': 'FormItem',
                    'x-component': 'InputNumber',
                    'x-component-props': {
                        size: 'small'
                    },
                    'x-reactions': {
                        dependencies: ['.rule_type'],
                        when: '{{$deps[0]==="多选"}}',
                        fulfill: {
                            schema: {
                                'x-visible': true,
                            },
                        },
                        otherwise: {
                            schema: {
                                'x-visible': false,
                            },
                        }
                    }
                },
                remark: {
                    type: 'string',
                    title: '备注',
                    'x-decorator': 'FormItem',
                    'x-component': 'Input.TextArea',
                    'x-component-props': {
                        size: 'small'
                    }
                },
                top_id: {
                    type: 'string',
                    visibel: false,
                },
                top_name: {
                    type: 'string',
                    visible: false,
                }
            },
        }
        return {
            schema,
        }
    },
    render(h) {
        return (
            <FormLayout labelCol={6} wrapperCol={10}>
                <SchemaField schema={this.schema} />
                <FormDialog.Footer>
                </FormDialog.Footer>
            </FormLayout>
        )
    },
}