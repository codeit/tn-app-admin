import { BaseApi } from "../../common/base-api";
import { doGet, doPost } from "../../common/network";
class EvaProjectUserApi extends BaseApi {
    constructor() {
        super({ prefix: "/eva-project-user" });
    }

    async findOwnedUser({ project_id }) {
        if (!project_id) throw new Error("project_id can not bu empty")
        const data = await doGet({ url: `${this.prefix}/find-owned-user`, data: { where: { project_id } } })
        return data;
    }
    async generateUser({ project_id, project_owner_id = undefined, amount = 0, user_only = false, test = false }) {
        if (!project_id) throw new Error("project_id can not bu empty")
        const data = await doPost({
            url: `${this.prefix}/generate-user`, data: {
                where: { project_id, project_owner_id, amount, user_only, test }
            }
        })
        return data;
    }
    async findUserByProject({ project_id, is_test }) {
        if (!project_id) throw new Error("project_id can not bu empty")
        const data = await this.findList({
            where: { project_id, is_test }
        });
        return data;
    }
}
export { EvaProjectUserApi }