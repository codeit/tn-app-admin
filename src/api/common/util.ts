function buildVirtualValue(values: string[]): string | undefined {
    if (values && values.length > 0) {
        return values.join("^");
    }
    return undefined;
}

export { buildVirtualValue };