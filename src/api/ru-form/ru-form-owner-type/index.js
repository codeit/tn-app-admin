import { BaseApi } from "../../common/base-api";
import { doPost, doGet } from "../../common/network";
class RuFormOwnerTypeApi extends BaseApi {
    constructor() {
        super({ prefix: "/ru-form-owner-type" });
    }

    async checkWeight({ form_id, group_type }) {
        const data = await doPost({ url: `${this.prefix}/check-weight`, data: { where: { form_id, group_type } } })
        return data;
    }
    async findFormsByType({ project_id, project_owner_id }) {
        const data = await doGet({ url: `${this.prefix}/find-forms-by-type`, data: { where: { project_id, project_owner_id } } })
        return data;
    }
}
export { RuFormOwnerTypeApi }