import { BaseApi } from "../../common/base-api";
import { doPost } from "../../common/network";
class RuFormRuleApi extends BaseApi {
    constructor() {
        super({ prefix: "/ru-form-rule" });
    }

    async checkWeight({ form_id }) {
        const data = await doPost({ url: `${this.prefix}/check-weight`, data: { where: { form_id } } })
        return data;
    }
}
export { RuFormRuleApi }