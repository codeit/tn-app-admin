import { BaseApi } from "../common/base-api";
class ObjectGroupApi extends BaseApi {
    constructor() {
        super({ prefix: "/object-group" });
    }

}
export { ObjectGroupApi }