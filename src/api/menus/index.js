import menus from "./header.json";
import amenus from "./header-n.json";

function getMenus(type) {
    return type === 0 ? menus : amenus;
}
export { getMenus }