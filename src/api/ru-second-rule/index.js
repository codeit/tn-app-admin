import { BaseApi } from "../common/base-api";
import { doGet } from "../common/network";
class RuSecondRuleApi extends BaseApi {
    constructor() {
        super({ prefix: "/ru-second-rule" });
    }

    async findTopRule({ id }) {
        const data = await doGet({ url: `${this.prefix}/find-top-rule`, data: { id } })
        return data;
    }
}
export { RuSecondRuleApi }