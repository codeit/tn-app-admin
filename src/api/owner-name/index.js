import { BaseApi } from "../common/base-api";
import { doGet } from "../common/network";
class OwnerNameApi extends BaseApi {
    constructor() {
        super({ prefix: "/owner-name" });
    }

    async findOwnersByProject({ project_id }) {
        if (!project_id) throw new Error("project_id can not bu empty")
        const data = await doGet({ url: `${this.prefix}/find-project-owners`, data: { where: { project_id } } })
        return data;
    }
}
export { OwnerNameApi }