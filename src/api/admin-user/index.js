import { BaseApi } from "../common/base-api";
import { doPost } from "../common/network";
import { z } from 'zod'
class AdminUserApi extends BaseApi {
    constructor() {
        super({ prefix: "/admin-user" });
    }

    async doLogin({ username, password }) {
        try {
            z
                .object({
                    password: z.string(),
                    username: z.string(),
                })
                .parse({ username, password });
        } catch (error) {
            throw error;
        }
        const data = await doPost({
            url: `${this.prefix}/login`, data: {
                where: { username, password }
            }
        })
        return data;
    }
    async changePassword({ username, password, password2, npassword }) {
        try {
            z.object({
                username: z.string(),
                password: z.string(),
                password2: z.string(),
                npassword: z.string(),
            })
                .refine((data) => {
                    return data.username
                }, {
                    message: "用户名不可用",
                    path: ["username"], // path of error
                })
                .refine((data) => data.password, {
                    message: "旧密码为空",
                    path: ['password']
                })
                .refine((data) => data.password2 === data.npassword, {
                    message: "两次输入的密码不一致",
                    path: ["password2", "npassword"]
                })
                .parse({ username, password, password2, npassword });
        } catch (error) {
            return {
                message: error.errors.map(e => e.message).join("<br/>")
            }
        }
        const result = await await doPost({
            url: `${this.prefix}/change-password`, data: {
                where: { username, password, password2, npassword }
            }
        })
        return result;
    }
}
export { AdminUserApi }