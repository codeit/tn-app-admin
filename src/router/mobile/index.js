import VueRouter from 'vue-router'
let router = new VueRouter({
    routes: [
        {
            path: "/",
            component: () => import("@mobile/mobile-vote/CheckUser")
        },
        {
            path: '/eva-mobile/home',
            name: 'mobile-vote-home',
            component: () => import("@mobile/mobile-vote/VoteHome")
        },
        {
            path: '/eva-mobile/vote-ok',
            name: 'vote-ok',
            component: () => import("@mobile/mobile-vote/VoteOk")
        },
        {
            path: '/eva-mobile/voted',
            name: 'vote-ed',
            component: () => import("@mobile/mobile-vote/VoteED")
        }
    ]
});
export { router }

