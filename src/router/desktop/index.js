import VueRouter from 'vue-router'
import { createGuard } from './guard';
let router = new VueRouter({
    routes: [
        {
            name: 'login',
            path: '/login',
            component: () => import("@desktop/login/Index")
        },
        {
            name: 'logout',
            path: '/logout',
            component: () => import("@desktop/logout/Index")
        },
        {
            path: '/',
            component: () => import("@desktop/nav/Index"),
            redirect: "/home",
            children: [
                {
                    path: 'home',
                    name: 'home',
                    component: () => import("@components/home/Home")
                },
                {
                    path: 'object-group',
                    component: () => import("@components/object-group/Index"),
                    children: [
                        {
                            path: 'pager',
                            component: () => import("@components/object-group/Pager")
                        },
                    ]
                },
                {
                    path: 'object-leader-type',
                    component: () => import("@components/object-leader-type/Index"),
                    children: [
                        {
                            path: 'pager',
                            component: () => import("@components/object-leader-type/Pager")
                        },
                    ]
                },
                {
                    path: 'ru-top-rule',
                    component: () => import("@components/ru-top-rule/Index"),
                    children: [
                        {
                            path: 'pager',
                            component: () => import("@components/ru-top-rule/Pager")
                        },
                    ]
                },
                {
                    path: 'ru-second-rule',
                    component: () => import("@components/ru-second-rule/Index"),
                    children: [
                        {
                            path: 'pager',
                            component: () => import("@components/ru-second-rule/Pager")
                        },
                    ]
                },
                {
                    path: 'owner-type',
                    component: () => import("@components/owner-type/Index"),
                    children: [
                        {
                            path: 'pager',
                            component: () => import("@components/owner-type/Pager")
                        },
                    ]
                },
                {
                    path: 'owner-name',
                    component: () => import("@components/owner-name/Index"),
                    children: [
                        {
                            path: 'pager',
                            component: () => import("@components/owner-name/Pager")
                        },
                    ]
                },
                {
                    path: 'ru-form',
                    component: () => import("@components/ru-form/Index"),
                    children: [
                        {
                            path: 'pager',
                            component: () => import("@components/ru-form/Pager")
                        },
                        {
                            name: "ru-form-setting",
                            path: 'setting',
                            component: () => import("@components/ru-form/Setting")
                        },
                    ]
                },
                {
                    path: 'eva-project',
                    component: () => import("@components/eva-project/Index"),
                    children: [
                        {
                            path: 'pager',
                            component: () => import("@components/eva-project/Pager")
                        },
                        {
                            path: 'result',
                            component: () => import("@components/eva-project/result/Pager")
                        },
                        {
                            name: "eva-project-setting",
                            path: 'setting',
                            component: () => import("@components/eva-project/Setting")
                        },
                    ]
                },
                {
                    path: 'eva-project-owner-form-user-setting',
                    name: "eva-project-owner-form-user-setting",
                    component: () => import("@components/eva-project/owner/form/UserSetting"),

                },
                {
                    path: 'eva-project/user/qrcode',
                    name: "qr-code",
                    component: () => import("@components/eva-project/user/Qrcode"),

                },
                {
                    path: 'admin-user/pager',
                    name: 'admin-user',
                    component: () => import("@components/admin-user/Pager")
                },
                {
                    path: 'admin-user/change-password',
                    name: 'change-password',
                    component: () => import("@components/admin-user/ChangePassword")
                }
            ]
        },

    ]
});
createGuard(router);
export { router }

