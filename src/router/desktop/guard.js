import { useLoginStore } from "../../store";
function createGuard(router) {
    router.beforeEach((to, from, next) => {
        const store = useLoginStore();
        if (store.logonId === null && from.name !== 'login' && to.name !== 'login') {
            next({ name: "login" })
        } else {
            next()
        }


    })
}
export { createGuard }